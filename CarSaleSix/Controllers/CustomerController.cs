﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CarSaleSix.Models;

namespace CarSaleSix.Controllers
{
    public class CustomerController : Controller
    {
        private CarsEntities db = new CarsEntities();

        // GET: CUSTOMERs
        public ActionResult Index()
        {
            return View(db.CUSTOMERS.ToList());
        }

        // GET: CUSTOMERs/Details/
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CUSTOMER customer = db.CUSTOMERS.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);
        }

        // GET: CUSTOMERs/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CUSTOMERs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "ISEMAILVERIFIED,ACTIVATIONCODE")]CUSTOMER customer)
        {
            bool Status = false;
            string Message = "";
            if (ModelState.IsValid)
            {


                #region Email Already Exists

                var isExist = IsEmailExist(customer.EMAIL);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExists","Email Already Exists");
                    return View(customer);
                }

                #endregion

                #region Generate Activation Code

                customer.ACTIVATIONCODE = Guid.NewGuid();

                #endregion

                #region Password Hashing

                customer.PASSWORD = Crypto.Hash(customer.PASSWORD);
                customer.CONFIRMPASSWORD = Crypto.Hash(customer.CONFIRMPASSWORD);

                #endregion

                customer.ISEMAILVERIFIED = false;

                #region Save to Database

                {
                    db.CUSTOMERS.Add(customer);
                    db.SaveChanges();

                    #region Send Email Verification link to User

                    SendVerificationLinkEmail(customer.EMAIL, customer.ACTIVATIONCODE.ToString());
                    Message =
                        "Registration Successfully Done. Account link activation link has been sent to your email id: " +
                        customer.EMAIL;
                    Status = true;
                    #endregion
                }

                #endregion

                //return RedirectToAction("Index");
            }
            else
            {
                Message = "Invalid Request";
            }

            ViewBag.Message = Message;
            ViewBag.Status = Status;

            return View(customer);
        }


        [NonAction]
        private bool IsEmailExist(string email)
        {
            var c = db.CUSTOMERS.FirstOrDefault(a => a.EMAIL == email);
            return c != null;
        }

        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor="VerifyAccount")
        {
            var verifyUrl = "/Customer/"+emailFor+"/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);
            var fromEmail = new MailAddress("carsalesdotnet@gmail.com", "Sales Gen 2018");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "qazwsxedcrfvtgbyhnujmikolp";
            string subject = "";
            string body = "";

            if (emailFor == "VerifyAccount")
            {
                 subject = "Your account is successfully created";
                 body =
                    "<br><br> We are excited to tell you that your Sales Gen account has been successfully created. Please click the below link to verify your account" +
                    "<br><a href='" + link + "'>" + link + "</a>";
            }else if (emailFor=="ResetPassword")
            {
                subject = "Reset Password";
                body =
                    "Hi, You requested for resetting the password of your Sales Gen account. Click on the below link to reset" +
                    "<br><br><a href=" + link + ">Reset Password</a>";
            }
           
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var Message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })

                smtp.Send(Message);

        }


        //Verify Account
        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool s = false;
   
           
                db.Configuration.ValidateOnSaveEnabled = false; //to avoid confirm password does not match issue
                var v = db.CUSTOMERS.Where(a => a.ACTIVATIONCODE == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.ISEMAILVERIFIED = true;
                    db.SaveChanges();
                    s = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }

            ViewBag.Status = s;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        //Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login login, string ReturnUrl="")
        {
            string Message = "";
            var v = db.CUSTOMERS.FirstOrDefault(a => a.EMAIL == login.EMAIL);
            if (v != null)
            {
                if (String.CompareOrdinal(Crypto.Hash(login.PASSWORD), v.PASSWORD) == 0)
                {
                    int timeout = login.RememberMe ? 10080 : 20;
                    var ticket = new FormsAuthenticationTicket(login.EMAIL, login.RememberMe, timeout);
                    string encrypt = FormsAuthentication.Encrypt(ticket);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName,encrypt);
                    cookie.Expires = DateTime.Now.AddMinutes(timeout);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    Message = "Invalid Credentials";
                }
            }
            else
            {
                Message = "Invalid Credentials";
            }
            ViewBag.Message = Message;
            return View();
        }

        
        //logout
        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage("Index");
            return RedirectToAction("Index", "Customer");
        }


        // GET: CUSTOMERs/Edit/
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CUSTOMER customer = db.CUSTOMERS.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);
        }

        // POST: CUSTOMERs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CUSTOMER customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        // GET: CUSTOMERs/Delete/
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CUSTOMER customer = db.CUSTOMERS.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);
        }

        // POST: CUSTOMERs/Delete/
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CUSTOMER customer = db.CUSTOMERS.Find(id);
            db.CUSTOMERS.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        //Forgot Password
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            //verify the email
            string Message = "";
            bool Status = false;
            var account = db.CUSTOMERS.Where(a => a.EMAIL == email).FirstOrDefault();
            if (account != null)
            {
                //send reset password link
                string resetCode = Guid.NewGuid().ToString();
                SendVerificationLinkEmail(account.EMAIL,resetCode,"ResetPassword");
                account.ResetPasswordCode = resetCode;
                db.Configuration.ValidateOnSaveEnabled = false;//for confirm password

                db.SaveChanges();
                Message = "Reset password link has been sent to your email";
            }
            else
            {
                Message = "Account not found";
            }

            ViewBag.Message = Message;
            return View();
        }

        //Reset Password
        public ActionResult ResetPassword(string id)
        {
            //verify password link
            //find account associated with this link
            //redirect to reset password page
            var user = db.CUSTOMERS.FirstOrDefault(a => a.ResetPasswordCode == id);
            if (user != null)
            {
                ResetPasswordModel model = new ResetPasswordModel();
                model.ResetCode = id;
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var Message = "";
            if (ModelState.IsValid)
            {
                var user = db.CUSTOMERS.FirstOrDefault(a => a.ResetPasswordCode == model.ResetCode);
                if (user != null)
                {
                    user.PASSWORD = Crypto.Hash(model.NewPassword);
                    user.ResetPasswordCode = "";
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    Message = "New Password Updated";
                }
            }
            else
            {
                Message = "Something went wrong";
            }
            ViewBag.Message = Message;
            return View(model);
        }
    }
}