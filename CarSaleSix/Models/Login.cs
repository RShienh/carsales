﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarSaleSix.Models
{
    public class Login
    {
        [Display(Name = "Email Address")]
        [Required(AllowEmptyStrings = false , ErrorMessage = "Email ID is required")]
        [DataType(DataType.EmailAddress)]
        public string EMAIL { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        public string PASSWORD { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }


    }
}