//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Web.Mvc;

namespace CarSaleSix.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class BILL
    {   [Key]
        public int BILLNO { get; set; }

        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is Required")]
        public string EMAIL { get; set; }

        [Display(Name = "Down Payment")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Down Payment is Required")]
        public decimal DOWN_PAYMENT { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true,DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime STARTDATE { get; set; }

        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ENDDATE { get; set; }

        public decimal INTEREST { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "EMI is Required")]
        public decimal EMI { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Total is Required")]
        public decimal TOTAL { get; set; }

        public IEnumerable<SelectListItem> FREQUENCY { get; set; }
    }
}
